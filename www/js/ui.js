let $ = jQuery;
let defaultImage = "img/taupe-default.jpg";
let cachedTaupes = null;

let renderTaupe = (taupe, i) => {
    let templateTaupe = `
        <div class="taupe" data-id="${i}">
            <img class="taupeImg" src="${defaultImage}"/>
            <div class="content">
                <h3 class="name">${taupe.name}</h3>
                <p class="desc">${taupe.description}</p>
                <p class="date">${taupe.date}</p>
                <p class="creator">${taupe.creator}</p>
            </div>
        </div>
    `;
    $('.taupes').append(templateTaupe);
}


function renderTaupes(newCachedTaupes='') {
    if (newCachedTaupes == '') {
        cachedTaupes = JSON.parse(localStorage.getItem("cached_taupes"));
        console.log("🚀 ~ file: ui.js ~ line 24 ~ renderTaupes ~ cachedTaupes", cachedTaupes);
        if (cachedTaupes != null) {
            cachedTaupes.reverse();
            cachedTaupes.forEach((taupe, i) => {
                renderTaupe(taupe, i);
            });
        }
    } else {
        newCachedTaupes.reverse();
        newCachedTaupes.forEach((taupe, i) => {
            renderTaupe(taupe, i);
        });
    }
}


function reRenderTaupesIfNecessary() {
    let newCachedTaupes = JSON.parse(localStorage.getItem("cached_taupes"));
    if (newCachedTaupes != cachedTaupes || cachedTaupes != null) {
        $(".taupes").empty();
        renderTaupes(newCachedTaupes);
    }
}
    
renderTaupes();