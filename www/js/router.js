$(document).ready(function() {
    const navs = $('.nav');

    /* Data */
    const itemsTitle = {
        'listTaupes': 'Les meilleures taupes du marché',
        'addTaupe': 'Ajouter une taupe',
        'detailTaupe': 'Le détail de la taupe'
    };

    /* Functions */
    function navigateTo(id) {
        navs.each(function(index, nav) {
            if (nav.id == id) {
                if (!$(this).hasClass("active")) {
                    $(this).addClass("active");
                }
                if (id == "addTaupe") {
                    alterPlusButton("show");
                } else {
                    alterPlusButton("hide");
                }
            } else {
                $(this).removeClass('active');
            }
        });
        changeTitle(id);
    }

    function alterPlusButton(state) {
        if (state == "show") {
            isShown = true;

            console.log('$("#addTaupe").offset().top', $("#addTaupe").offset().top);

            let offset = $("#addTaupe").offset().top + $("#addTaupe").height;

            $(".addBtn").addClass("formed");
            $('.addBtn').css("bottom", offset + "px");
            $(".addBtnText").addClass("shown");
            $(".addBtn > .plus").addClass("hidden");
        } else {
            isShown = false;
            $(".addBtn").removeClass("formed");
            $('.addBtn').css("bottom", "25px");
            $(".addBtnText").removeClass("shown");
            $(".addBtn > .plus").removeClass("hidden");
        }
    }

    function changeTitle(id) {
        $('.title').text(itemsTitle[id]);
    }

    function submitForm() {
        let taupeName = $('#taupeName').val();
        let taupeDescription = $('#taupeDescription').val();
        let taupeCreator = $('#taupeCreator').val();

        let taupeToAdd = {
            "name": taupeName, 
            "description": taupeDescription, 
            "date": (new Date()).toLocaleDateString('fr-FR'),
            "creator": taupeCreator
        }

        let allTaupes = JSON.parse(localStorage.getItem("cached_taupes"));

        allTaupes.push(taupeToAdd);
        localStorage.setItem("cached_taupes", JSON.stringify(allTaupes));

        $('#taupeName').val('');
        $('#taupeDescription').val('');
        $('#taupeCreator').val('');

        navigateTo("listTaupes");

        reRenderTaupesIfNecessary();
    }

    function insertTaupeDetails(id) {
        let taupeDetails = JSON.parse(localStorage.getItem("cached_taupes"))[id];
        $('.detailName').text(taupeDetails.name);
        $('.detailDesc').text(taupeDetails.date);
        $('.detailDate').text(taupeDetails.date);
        $('.detailCreator').text(taupeDetails.creator);
        $('.detailInfosLink').click(function() {
            openInAppBrowser("https://en.wikipedia.org/wiki/Taupe");
        });
    }

    function insertCreatorsIfNecessary() {
        if (!$("#taupeCreator").has('option').length > 0) {
            let creators = JSON.parse(localStorage.getItem("cached_taupe_creators"));
            console.log("🚀 ~ file: router.js ~ line 91 ~ insertCreatorsIfNecessary ~ creators", creators)
            creators.forEach((creator) => {
                $("#taupeCreator").append(`
                    <option value='${creator.name}'>${creator.name}</option>
                `);
            });
        }
    }

    /* Bind actions */
    $('.addBtn').click(function() {
        if ($(this).hasClass("formed")) {
            console.log("launched");
            submitForm();
        } else {
            navigateTo('addTaupe');
            insertCreatorsIfNecessary();
        }
    });
    $('.logo').click(function() {
        navigateTo('listTaupes');
    });
    $('.taupe').click(function() {
        if (!$(this).parent().is("#detailTaupe")) {
            let taupeId = $(this).data('id');
            navigateTo('detailTaupe');
            insertTaupeDetails(taupeId);
        }
    });
});