// cordova-plugin-inappbrowser (Custom browser app + jQuery scripts)
// cordova-plugin-battery-status (Simple log)
// cordova-plugin-screen-orientation (Lock to Portrait)
// cordova-plugin-network-information (Modal if no connection)
// cordova-plugin-geolocation (Show lat / long / altitude and handle refuse)
// cordova-plugin-splashscreen (Loading screen fixed to 2000 ms)
// cordova-plugin-statusbar (custom color of StatusBar)


function onBatteryStatus(status) {
    console.log("🚀 ~ file: native.js ~ line 2 ~ onBatteryStatus ~ status", status);
}

const openInAppBrowserOptions = "location=no,zoom=false,footer=yes,hideurlbar=yes,hidenavigationbuttons=yes,hardwareback=yes";

const openInAppBrowser = (link) => {
    var payload = `jQuery('body > div').prepend('<p class="topbar">' + jQuery("#section_0").text() + '</p>');`;
    var inAppRef = cordova.InAppBrowser.open(link, "_blank", openInAppBrowserOptions);
    inAppRef.addEventListener('loadstop', function (event) {
        inAppRef.executeScript({ code: payload });
        inAppRef.insertCSS({ code: ".topbar{height:50px!important;background-color:#000000;color:#fff;font-size:25px;text-align:center;padding-top:15px;}" });
    });
};

const deviceReady = () => {

    let today = (new Date()).toLocaleDateString('fr-FR');

    let taupes = localStorage.getItem("cached_taupes");
    if (taupes == null) {
        let newTaupes = [
            {
                "name": "Taupe Modèle",
                "description": "Elle est au taupe !",
                "date": today,
                "creator": "JeanTaupe"
            },
            {
                "name": "Taupe Gun",
                "description": "C'est une super taupe celle là !",
                "date": today,
                "creator": "JeanTaupe"
            },
            {
                "name": "Roof Taupe",
                "description": "Petite taupe en altitude :)",
                "date": today,
                "creator": "JeanTaupe"
            },
            {
                "name": "Marc Taupe Piqueur",
                "description": "Petite taupe dure de la tête",
                "date": today,
                "creator": "JeanTaupe"
            }
        ];
        localStorage.setItem("cached_taupes", JSON.stringify(newTaupes));
        reRenderTaupesIfNecessary();
    }

    let creators = localStorage.getItem("cached_taupe_creators");
    if (creators == null) {
        let newCreators = [
            {
                "name": "Roof Taupe",
                "inscription": today
            },
            {
                "name": "Taupe Innambour",
                "inscription": today
            },
            {
                "name": "Crop Taupe",
                "inscription": today
            }
        ];
        localStorage.setItem("cached_taupe_creators", JSON.stringify(newCreators));
    }
};


// Block l'app si pas de connection --> Modal
const checkConnection = () => {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';

    console.log("aaaaaaaaaa", states[networkState]);

    if (states[networkState] == 'No network connection') {
        jQuery('body').addClass('modal-block');
    }
}


// Verrouille l'app en format portrait
const lockPortait = () => {
    window.screen.orientation.lock('portrait');
}

const navigatorHandle = () => {
    var onSuccess = function(position) {
        jQuery('.localisation').append(`
            <p>Latitude : ${position.coords.latitude}</p>
            <p>Longitude : ${position.coords.longitude}</p>
            <p>Altitude : ${position.coords.altitude}</p>
        `);
        jQuery('.localisation > span').click(function() {
            console.log("CLICKEEEEEEEEEEEEEEEED");
            if (jQuery(this).hasClass('open')) {
                jQuery('.localisation p').css("display", "block");
                jQuery(this).removeClass("open");
                jQuery(this).addClass('close');
            } else {
                jQuery('.localisation p').css("display", "none");
                jQuery('.localisation span').css("display", "none");
                jQuery(this).removeClass("close");
                jQuery(this).addClass('open');
            }
        })
    };

    function onError(error) {
        jQuery('.localisation').append(`
            <p>Code d'erreur (localisation) : ${error.code}</p>
            <p>Message d'erreur (localisation) : ${error.message}</p>
        `);
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
}